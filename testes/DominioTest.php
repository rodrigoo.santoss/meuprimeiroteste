<?php 
use PHPUnit\Framework\TestCase;

final class DominioTest extends TestCase{

    public function testValidaDominioVazioDeveRetornarVazia(){
        $dominio=new Dominio("");
        $this-> assertTrue($dominio -> validaDominioVazio());
        
        $dominio2=new Dominio("www.kinghost.com.br");
        $this-> assertFalse($dominio2 -> validaDominioVazio());
    }

   public function testRetiraEspacosEmBranco(){
       $dominio=new Dominio("alo testes ");
         
        $dominio=$dominio -> retiraEspacos();
        $this -> assertFalse(strpos($dominio, " "));
    }

   public function testValidaMinimoCaracter(){
        $dominio=new Dominio("x");
        $this-> assertFalse($dominio -> minimoCaracteres());

        $dominio=new Dominio("xxx");
        $this-> assertTrue($dominio -> minimoCaracteres());
    }

    public function testValidaMaximoCaracter(){
        $dominio=new Dominio("xxxxxxxxxxxxxxxxxxxxxxxxxx
        xxxxxxx");
        $this-> assertFalse($dominio -> maximoCaracteres());

        $dominio=new Dominio("xxxxx");
        $this-> assertTrue($dominio -> maximoCaracteres());
    }   

    public function testValidarSomenteNumeros(){
        $dominio=new Dominio("111111");
        $this-> assertTrue($dominio -> somenteNumeros());

        $dominio2=new Dominio("www.rodrigocaetano.com.br");
        $this-> assertFalse($dominio2 -> somenteNumeros());

    }

}